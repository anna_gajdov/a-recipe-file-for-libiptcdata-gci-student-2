SUMMARY="The Boehm-Demers-Weiser conservative garbage collector"
DESCRIPTION="
The Boehm-Demers-Weiser conservative garbage collector can be used as
a garbage collecting replacement for C malloc or C++ new. It allows
you to allocate memory basically as you normally would, without
explicitly deallocating memory that is no longer useful. The collector
automatically recycles memory when it determines that it can no longer
be otherwise accessed."

HOMEPAGE="http://www.hpl.hp.com/personal/Hans_Boehm/gc/"
SRC_URI="http://www.hpl.hp.com/personal/Hans_Boehm/gc/gc_source/gc-7.2d.tar.gz"
REVISION="1"
ARCHITECTURES="x86 x86_gcc2"
CHECKSUM_MD5="91340b28c61753a789eb6077675d87d2"
SOURCE_DIR="gc-7.2"

PATCHES="boehm_gc-7.2d.patch"

PROVIDES="
	boehm_gc = $portVersion compat >= 7
	lib:libgc = $portVersion compat >= 7
	lib:libcord = $portVersion compat >= 7
	"

REQUIRES="
	haiku >= $haikuVersion
	"

BUILD_REQUIRES=""

BUILD_PREREQUIRES="
	haiku_devel >= $haikuVersion
	cmd:libtoolize
	cmd:autoconf
	cmd:make
	cmd:gcc
	cmd:ld
	"

PROVIDES_devel="
	boehm_gc_devel = $portVersion compat >= 7
	devel:libgc = $portVersion compat >= 7
	devel:libcord = $portVersion compat >= 7
	"

REQUIRES_devel="
	boehm_gc == $portVersion
	"

BUILD()
{
	libtoolize --force --copy --install
	autoconf -f
	runConfigure ./configure --enable-threads=posix \
	--enable-thread-local-alloc --enable-parallel-mark
	make
}

INSTALL()
{
	make install
	prepareInstalledDevelLibs libgc libcord
	fixPkgconfig
	packageEntries devel $developDir
}

TEST()
{
	make check
}

LICENSE="BOEHM"
COPYRIGHT="1988, 1989 Hans-J. Boehm, Alan J. Demers
	1991-1996 by Xerox Corporation.  All rights reserved.
	1996-1999 by Silicon Graphics.  All rights reserved.
	1999-2011 Hewlett-Packard Development Company, L.P."
