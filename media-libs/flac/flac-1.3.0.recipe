SUMMARY="flac - Free Lossless Audio Codec"
DESCRIPTION="FLAC stands for Free Lossless Audio Codec, an audio format similar to MP3, but lossless, meaning that audio is compressed in FLAC without any loss in quality.  This is similar to how Zip works, except with FLAC you will get much better compression because it is designed specifically for audio, and you can play back compressed FLAC files in your favorite player (or your car or home stereo) just like you would an MP3 file." 
HOMEPAGE="http://flac.sourceforge.net/" 
SRC_URI="http://downloads.xiph.org/releases/flac/flac-1.3.0.tar.xz"
CHECKSUM_MD5="13b5c214cee8373464d3d65dee362cdd"
REVISION="1"

LICENSE="GNU LGPL v2.1
	GNU GPL v2
	BSD (3-clause)
	"
	
COPYRIGHT="2000-2007 Josh Coalson"

ARCHITECTURES="x86 x86_gcc2"

PROVIDES="
	flac$secondaryArchSuffix = $portVersion compat >= 1
	libFLAC$secondaryArchSuffix = 1.3.0 compat >= 1
	cmd:flac
	cmd:metaflac 
	lib:libFLAC++$secondaryArchSuffix
	lib:libFLAC$secondaryArchSuffix
	"
	
REQUIRES="
	haiku
	lib:libiconv
	lib:libogg
	"
	
BUILD_REQUIRES="
	"
	
BUILD_PREREQUIRES="
	haiku${secondaryArchSuffix}_devel
	cmd:nasm
	cmd:gcc$secondaryArchSuffix
	cmd:libtoolize
	cmd:aclocal
	cmd:autoconf
	cmd:automake
	cmd:ld$secondaryArchSuffix
	cmd:make
	cmd:gawk
	cmd:perl
	devel:libiconv
	devel:libogg
	"

BUILD()
{
	libtoolize --force --copy --install
	aclocal
	autoconf
	runConfigure ./configure
	make $jobArgs
}

INSTALL()
{
	make install
		
	prepareInstalledDevelLibs libFLAC
	fixPkgconfig
	
	# devel package
	packageEntries devel \
		$developDir

}

TEST()
{
	test_grabbag.sh
	test_streams.sh
	test_libFLAC.sh
	test_wrapper.sh
	test_bins.sh
	test_libFLAC++.sh
	test_compression.sh
	test_metaflac.sh
	test_flac.sh
	test_seeking.sh
}

# ----- devel package -------------------------------------------------------

PROVIDES_devel="
	flac${secondaryArchSuffix}_devel = $portVersion
	libFLAC${secondaryArchSuffix}_devel = $portVersion
	devel:flac$secondaryArchSuffix = 8.3.0 compat >= 8
	devel:libFLAC$secondaryArchSuffix = 8.3.0 compat >= 8
	devel:libFLAC++$secondaryArchSuffix = 8.3.0 compat >= 8
	"

REQUIRES_devel="
	libFLAC$secondaryArchSuffix == $portVersion
	"
